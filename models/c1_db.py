#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 20.10.2014
# Purpose: Модель - Определения таблиц БД
#############################################################################
#                uCommun - Управление коммуникаторами
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os
from gluon.dal import DAL, Field
from gluon.validators import *
from gluon.tools import *
import cLib as l
#from gluon import current
#############################################################################
# Имя СУБД
SUBD  = "sqlite"
#SUBD = "postgres"
#SUBD = "mysql"
#----------------------------------------------------------------------------
# Определение имени хоста по наличию переменной окружения "VCAP_SERVICES"
HOST = "cloud" if os.getenv("VCAP_SERVICES") else "localhost"
#----------------------------------------------------------------------------
if SUBD == "sqlite":
    # Используется СУБД SQLite
    #------------------------------------------------------------------------
    # Создание словаря с параметрами доступа к серверу SQLite на localhost
    dCred = dict(
        subd = SUBD,
        user = "",
        password = "",
        host = "",
        name = "ucommun.sqlite"
    )  
    #------------------------------------------------------------------------
    # Универсальный идентификатор ресурса
    URI = "%(subd)s://%(name)s" % dCred
    #------------------------------------------------------------------------
    # Каталог для файлов (*.table) с метаданными
    PATH = "applications/ucommun/databases/SQLite"
    #------------------------------------------------------------------------
elif SUBD == "postgres":
    # Используется СУБД PostgreSQL
    #------------------------------------------------------------------------
    if HOST == "localhost":
        # Используется локальный компьютер
        #--------------------------------------------------------------------
        # Создание словаря с параметрами доступа к серверу PgSQL на localhost
        dCred = dict(
            subd = SUBD,
            user = "postgres",
            password = "cj.pybr",
            host = HOST,
            name = "ucommun"
        )
        #--------------------------------------------------------------------
        # Универсальный идентификатор ресурса
        URI = "%(subd)s://%(user)s:%(password)s@%(host)s/%(name)s" % dCred
        #--------------------------------------------------------------------
        # Каталог для файлов (*.table) с метаданными
        PATH = "applications/ucommun/databases/PgSQL"
        #--------------------------------------------------------------------
    elif HOST == "cloud":
        # Используется виртуальный компьютер из "облачного" хостинга AppFog
        #--------------------------------------------------------------------
        # Создание словаря с параметрами доступа к серверу PgSQL на AppFog
        dCred = l.Cred("postgresql-9.1")
        #--------------------------------------------------------------------
        # Добавление в словарь dCred элемента subd
        dCred["subd"] = SUBD
        #--------------------------------------------------------------------
        # Универсальный идентификатор ресурса
        URI = "%(subd)s://%(user)s:%(password)s@%(host)s/%(name)s" % dCred
        #--------------------------------------------------------------------
        # Каталог для файлов (*.table) с метаданными
        PATH = "applications/ucommun/databases/PgSQL"
    #endif    
    #------------------------------------------------------------------------
elif SUBD == "mysql":
    # Используется СУБД MySQL
    #------------------------------------------------------------------------
    # Создание словаря с параметрами доступа к серверу MySQL на localhost
    dCred = dict(
        subd = SUBD,
        user = "root",
        password = "dba",
        host = "localhost",
        name = "ucommun"
    )
    #------------------------------------------------------------------------
    # Универсальный идентификатор ресурса
    URI = "%(subd)s://%(user)s:%(password)s@%(host)s/%(name)s" % dCred
    #------------------------------------------------------------------------
    # Каталог для файлов (*.table) с метаданными
    PATH = "applications/ucommun/databases/MySQL"
#endif
#----------------------------------------------------------------------------
# Объект соединения с основной БД
db = DAL(
    # Строка соединения с основной БД
    URI,
    #------------------------------------------------------------------------
    # Этот каталог должен существовать до момента создания БД
    #folder = os.path.join(os.getcwd(), PATH)
    folder = PATH
)
#############################################################################
#
#               С П Р А В О Ч Н Ы Е   Т А Б Л И Ц Ы
#
#############################################################################
# Главная справочная таблица - ОБЪЕКТЫ
db.define_table( 
    "obj",  
    Field("snaim", "string", length=15, default="", unique=True),
    Field("naim", "string", length=512, default="", notnull=True),
    Field("address", "string", length=200, default=""),
    Field("ready", "boolean", default=False, notnull=True),
    format = "%(snaim)s",
    migrate='obj.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.obj.snaim.label = "СокрНаимОбъекта"
db.obj.naim.label = "НаимОбъекта"
db.obj.address.label = "АдрОбъекта"
db.obj.ready.label = "Опросить"
#============================================================================
# Подчинённая справочная таблица - ШИНЫ 
db.define_table( 
    "bus",
    Field("obj_id", "reference obj"), 
    Field("snaim", "string", length=30, default="", unique=True),
    Field("naim", "string", length=256, default="", notnull=True),
    Field("host", "string", length=15, default="", notnull=True),
    Field("port", "string", length=7, default="", notnull=True),
    Field("timeout", "integer"),
    Field("ready", "boolean", default=False, notnull=True),
    format = "%(snaim)s",
    migrate='bus.table',
)    
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.bus.obj_id.label = "Объект"
db.bus.snaim.label = "СокрНаимШины"
db.bus.naim.label = "НаимШины"
db.bus.host.label = "IpАдрTCPсервера"
db.bus.port.label = "ПортTCPсервера"
db.bus.timeout.label = "Тайм-аут"
db.bus.ready.label = "Опросить"
#============================================================================
'''
# Подчинённая справочная таблица - КОРПУСА
db.define_table( 
    "krp",  
    Field("obj_id", "reference obj"),
    Field("snaim", "string", length=10, default="", unique=True),
    Field("naim", "string", length=256, default="", notnull=True),
    Field("lim", "decimal(10,2)", notnull=True),
    format = "%(snaim)s",
    migrate='krp.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.krp.obj_id.label = "Объект"
db.krp.snaim.label = "СокрНаимКорп"
db.krp.naim.label = "НаимКорпуса"
db.krp.lim.label = "Лимит"
#============================================================================
# Подчинённая справочная таблица - ПОДРАЗДЕЛЕНИЯ
db.define_table( 
    "pdr", 
    Field("obj_id", "reference obj"), 
    Field("snaim", "string", length=100, default="", unique=True),
    Field("naim", "string", length=256, default="", notnull=True),
    Field("lim", "decimal(10,2)", notnull=True),
    format = "%(snaim)s",
    migrate='pdr.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.pdr.obj_id.label = "Объект"
db.pdr.snaim.label = "СокрНаимПодразд"
db.pdr.naim.label = "НаимПодразд"
db.pdr.lim.label = "Лимит"
#============================================================================
# Cправочная таблица - НАЗНАЧЕНИЕ ЭНЕРГИИ
db.define_table(
    "lgh",
    Field("obj_id", "reference obj"), 
    Field("snaim", "string", length=30, default="", unique=True),
    Field("naim", "string", length=256, default="", notnull=True),
    format = "%(snaim)s",
    migrate='lgh.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.lgh.obj_id.label = "Объект"
db.lgh.snaim.label = "СокрНаимНазнЭн"
db.lgh.naim.label = "НаимНазнЭнер"
#============================================================================
'''
# Подчинённая справочная таблица - СЧЁТЧИКИ
db.define_table( 
    "com",  
    Field("bus_id", 'reference bus'), 
    Field("addr", "string", length=2, notnull=True),
    Field("ready", "boolean", default=False, notnull=True),
    format = "%(addr)s",
    migrate='com.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.com.bus_id.label = "Шина"
db.com.addr.label = "АдрСч"
db.com.ready.label = "Опросить"
'''
#============================================================================
# Подчинённая справочная таблица - СЧЁТЧИКИ
db.define_table( 
    "sht",  
    Field("bus_id", 'reference bus'), 
    Field("krp_id", 'reference krp'), 
    Field("pdr_id", 'reference pdr'), 
    Field("lgh_id", 'reference lgh'), 
    Field("addr", "string", length=2, notnull=True),
    Field("ready", "boolean", default=False, notnull=True),
    format = "%(addr)s",
    migrate='sht.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.sht.bus_id.label = "Шина"
db.sht.krp_id.label = "Корпус"
db.sht.pdr_id.label = "Подразд"
db.sht.lgh_id.label = "Назнач"
db.sht.addr.label = "АдрСч"
db.sht.ready.label = "Опросить"
#============================================================================
# Cправочная таблица - КОДЫ ЗАПРОСОВ
db.define_table(
    "cdz",
    Field("codz", "string", length=2, default="", notnull=True),
    Field("naim", "string", length=150, default="", notnull=True),
    format = "%(naim)s",
    migrate='cdz.table',
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.cdz.codz.label = "КодЗапр"
db.cdz.naim.label = "НаимЗапр"
#============================================================================
# Cправочная таблица - КОДЫ ПАРАМЕТРОВ ЗАПРОСОВ
db.define_table(
    "cdp",
    Field("codz", "string", length=2, default=""),
    Field("codp", "string", length=2, default=""),
    Field("cdz_id", "reference cdz"),
    Field("naim", "string", length=512, default=""),
    format = "%(naim)s",
    migrate='cdp.table',
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.cdp.codz.label = "КодЗапр"
db.cdp.codp.label = "КодПарЗапр"
db.cdp.cdz_id.label = "КодЗапр"
db.cdp.naim.label = "НаимПарЗапр"
#============================================================================
# Справочная таблица - ПАРАМЕТРЫ ЗАПРОСОВ
db.define_table(
    "par",
    Field("codz", "string", length=2, default="", notnull=True),
    Field("codp", "string", length=2, default=""),
    Field("par", "string", length=20, default=""),
    Field("cdz_id", "reference cdz"),
    Field("cdp_id", "reference cdp"), 
    Field("snaim", "string", length=50, default=""),
    Field("naim", "string", length=256, default=""),
    Field("yb", "string", length=2, default=""),
    Field("ye", "string", length=2, default=""),
    Field("typ", "string", length=2, default=""),
    Field("col", "string", length=30, default=""),
    format = "%(snaim)s",
    migrate="par.table"
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.par.codz.label = "КодЗапр"
db.par.codp.label = "КодПар"
db.par.par.label = "Парам"
db.par.cdz_id.label = "КодЗапр"
db.par.cdp_id.label = "КодПар"
db.par.snaim.label = "СокрНаим"
db.par.naim.label = "ПолнНаим"
db.par.yb.label = "НомНачБайта"
db.par.ye.label = "НомКонБайта"
db.par.typ.label = "ТипПарам"
db.par.col.label = "НаимКол"
#============================================================================
# Главная справочная таблица - ТАРИФЫ
db.define_table( 
    "trf",
    Field("ntrf", "string", length=1, default="", unique=True),
    Field("snaim", "string", length=30, default="", notnull=True),
    Field("time_beg", "string", length=8, default="", notnull=True),
    Field("time_end", "string", length=8, default="", notnull=True),
    migrate='trf.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.trf.ntrf.label = "НомТарифа"
db.trf.snaim.label = "СокрНаимТарифа"
db.trf.time_beg.label = "ВрНачТарифа"
db.trf.time_end.label = "ВрОкнТарифа"
#############################################################################
#
#         О П Е Р А Т И В Н Ы Е  (ПОДЧИНЁННЫЕ)  Т А Б Л И Ц Ы
#
#############################################################################
# Оперативная таблица - БАЗОВЫЕ ПАРАМЕТРЫ СЧЁТЧИКОВ
db.define_table( 
    "osht", 
    Field("obj_id", "reference obj"),
    Field("bus_id", "reference bus"),
    Field("sht_id", "reference sht", unique=True),
    Field("addr", "string", length=3, default="", notnull=True),
    Field("l_addr", "string", length=10, default="", notnull=True),
    Field("ser_num", "string", length=10, default="", notnull=True),
    Field("tip", "string", length=50, default="", notnull=True),
    Field("ver_po", "string", length=8, default="", notnull=True),
    Field("point", "string", length=16, default="", notnull=True),
    Field("date_izg", "string", length=10, default="", notnull=True),
    Field("edi", "string",length=5, default="", notnull=True),
    Field("tmpr", "string", length=5, default="", notnull=True),
    Field("k_tr_u", "string", length=5, default="", notnull=True),
    Field("k_tr_i", "string", length=5, default="", notnull=True),
    Field("k_tr_cur", "string", length=10, default="", notnull=True),
    Field("cla", "string", length=10, default="", notnull=True),
    Field("clr", "string", length=10, default="", notnull=True),
    Field("unom", "string", length=20, default="", notnull=True),
    Field("inom", "string", length=3, default="", notnull=True),
    Field("aconst", "string", length=10, default="", notnull=True),
    Field("tmp", "string", length=5, default="", notnull=True),
    Field("faz", "string", length=1, default="", notnull=True),
    Field("t_fix", "string", length=19, default="", notnull=True),
    Field("ti1", "string", length=2, default="", notnull=True),
    Field("date1", "string", length=10, default="", notnull=True),
    Field("time1", "string", length=5, default="", notnull=True),
    Field("pntr1", "string", length=4, default="", notnull=True),
    Field("ti2", "string", length=2, default="", notnull=True),
    Field("date2", "string", length=10, default="", notnull=True),
    Field("time2", "string", length=5, default="", notnull=True),
    Field("pntr2", "string", length=4, default="", notnull=True),
    Field("ti3", "string", length=2, default="", notnull=True),
    Field("date3", "string", length=10, default="", notnull=True),
    Field("time3", "string", length=5, default="", notnull=True),
    Field("pntr3", "string", length=4, default="", notnull=True),
    Field("cnl", "string", length=50, default="", notnull=True),
    Field("isp", "string", length=100, default="", notnull=True),
    Field("npr", "string", length=3, default="", notnull=True),
    Field("pam", "string", length=3, default="", notnull=True),
    Field("kolmas", "string", length=1, default="", notnull=True),
    Field("mas", "string", length=100, default="", notnull=True),
    Field("par", "string", length=150, default="", notnull=True),
    Field("k", "string", length=5, default="", notnull=True),
    Field("rs485", "string", length=5, default="", notnull=True),
    Field("ist", "string", length=10, default="", notnull=True),
    Field("can", "string", length=10, default="", notnull=True),
    Field("ident", "string", length=32, default="", notnull=True),
    Field("type_ks", "string", length=3, default="", notnull=True),
    Field("d_lv", "string", length=10, default="", notnull=True),
    Field("h_lv", "string", length=2, default="", notnull=True),
    Field("d_zv", "string", length=10, default="", notnull=True),
    Field("h_zv", "string", length=2, default="", notnull=True),
    Field("find_m", "string", length=2, default="", notnull=True),
    Field("find_p", "string", length=2, default="", notnull=True),
    Field("find_a", "string", length=5, default="", notnull=True),
    Field("bszp", "string", length=2, default="", notnull=True),
    Field("d_tt", "string", length=10, default="", notnull=True),
    Field("t_tt", "string", length=5, default="", notnull=True),
    Field("n_tt", "string", length=1, default="", notnull=True),
    Field("l_tt", "string", length=1, default="", notnull=True),
    Field("freq", "string", length=5, default="", notnull=True),
    Field("mnoj", "string", length=3, default="", notnull=True),
    Field("cur485", "string", length=1, default="", notnull=True),
    Field("baudrate", "string", length=6, default="", notnull=True),
    Field("parity", "string", length=1, default="", notnull=True),
    Field("npu", "string", length=2, default="", notnull=True),
    Field("mse", "string", length=19, default="", notnull=True),
    migrate='osht.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.osht.obj_id.label = "Объект"
db.osht.bus_id.label = "Шина"
db.osht.sht_id.label = "АдресСч"
db.osht.addr.label = "КорАдр"
db.osht.l_addr.label = "РасшАдр"
db.osht.ser_num.label = "СерНом"
db.osht.tip.label = "ТипСЧ"
db.osht.ver_po.label = "ВерсияПО"
db.osht.point.label = "ТочУчёта"
db.osht.date_izg.label = "ДатаИзг"
db.osht.edi.label = "ЕдИзм"
db.osht.tmpr.label = "Темпер"
db.osht.k_tr_u.label = "КтрНапр"
db.osht.k_tr_i.label = "КтрТок"
db.osht.k_tr_cur.label = "КтрТек"
db.osht.cla.label = "КлассАкт"
db.osht.clr.label = "КлассРеак"
db.osht.unom.label = "Uном"
db.osht.inom.label = "Iном"
db.osht.aconst.label = "Аконст"
db.osht.tmp.label = "Тдиап"
db.osht.faz.label = "Фаз"
db.osht.t_fix.label = "МомФикс"
db.osht.ti1.label = "ВрИнтег1"
db.osht.date1.label = "ДтТекСр1"
db.osht.time1.label = "ВрТекСр1"
db.osht.pntr1.label = "АдТекСр1"
db.osht.ti2.label = "ВрИнтег2"
db.osht.date2.label = "ДтТекСр2"
db.osht.time2.label = "ВрТекСр2"
db.osht.pntr2.label = "АдТекСр2"
db.osht.ti3.label = "ВрИнтег3"
db.osht.date3.label = "ДтТекСр3"
db.osht.time3.label = "ВрТекСр3"
db.osht.pntr3.label = "АдТекСр3"
db.osht.cnl.label = "ТипКанУч"
db.osht.isp.label = "Исполн"
db.osht.npr.label = "НапрУч"
db.osht.pam.label = "РазмерПам"
db.osht.kolmas.label = "КолМас"
db.osht.mas.label = "ХарМас"
db.osht.par.label = "ПарКомпл"
db.osht.k.label = "Каналов"
db.osht.rs485.label = "RS485"
db.osht.ist.label = "РезИст"
db.osht.can.label = "CAN"
db.osht.ident.label = "Идентиф"
db.osht.type_ks.label = "ТипКС"
db.osht.d_lv.label = "ДатаЛВ"
db.osht.h_lv.label = "ЧасЛВ"
db.osht.d_zv.label = "ДатаЗВ"
db.osht.h_zv.label = "ЧасЗВ"
db.osht.find_m.label = "НайдМассив"
db.osht.find_p.label = "НайдСтран"
db.osht.find_a.label = "НайдАдрес"
db.osht.bszp.label = "СостЗадачи"
db.osht.d_tt.label = "ДатТТ"
db.osht.t_tt.label = "ВрТТ"
db.osht.n_tt.label = "НомТТ"
db.osht.l_tt.label = "ЛимТТ"
db.osht.freq.label = "Частота"
db.osht.mnoj.label = "Множ485"
db.osht.cur485.label = "НомТек485"
db.osht.baudrate.label = "СкорШины"
db.osht.parity.label = "Паритет"
db.osht.npu.label = "ЧислоПерУср"
db.osht.mse.label = "МомСбр"
#============================================================================
'''
# Оперативная таблица - БАЗОВЫЕ ПАРАМЕТРЫ КОММУНИКАТОРОВ
db.define_table( 
    "ocom", 
    Field("obj_id", "reference obj"),
    Field("bus_id", "reference bus"),
    Field("com_id", "reference com", unique=True),
    Field("addr", "string", length=3, default="", notnull=True),
    Field("l_addr", "string", length=10, default="", notnull=True),
    Field("ser_num", "string", length=10, default="", notnull=True),
    Field("tip", "string", length=50, default="", notnull=True),
    Field("ver_po", "string", length=8, default="", notnull=True),
    Field("point", "string", length=16, default="", notnull=True),
    Field("date_izg", "string", length=10, default="", notnull=True),
    Field("edi", "string",length=5, default="", notnull=True),
    Field("tmpr", "string", length=5, default="", notnull=True),
    Field("k_tr_u", "string", length=5, default="", notnull=True),
    Field("k_tr_i", "string", length=5, default="", notnull=True),
    Field("k_tr_cur", "string", length=10, default="", notnull=True),
    Field("cla", "string", length=10, default="", notnull=True),
    Field("clr", "string", length=10, default="", notnull=True),
    Field("unom", "string", length=20, default="", notnull=True),
    Field("inom", "string", length=3, default="", notnull=True),
    Field("aconst", "string", length=10, default="", notnull=True),
    Field("tmp", "string", length=5, default="", notnull=True),
    Field("faz", "string", length=1, default="", notnull=True),
    Field("t_fix", "string", length=19, default="", notnull=True),
    Field("ti1", "string", length=2, default="", notnull=True),
    Field("date1", "string", length=10, default="", notnull=True),
    Field("time1", "string", length=5, default="", notnull=True),
    Field("pntr1", "string", length=4, default="", notnull=True),
    Field("ti2", "string", length=2, default="", notnull=True),
    Field("date2", "string", length=10, default="", notnull=True),
    Field("time2", "string", length=5, default="", notnull=True),
    Field("pntr2", "string", length=4, default="", notnull=True),
    Field("ti3", "string", length=2, default="", notnull=True),
    Field("date3", "string", length=10, default="", notnull=True),
    Field("time3", "string", length=5, default="", notnull=True),
    Field("pntr3", "string", length=4, default="", notnull=True),
    Field("cnl", "string", length=50, default="", notnull=True),
    Field("isp", "string", length=100, default="", notnull=True),
    Field("npr", "string", length=3, default="", notnull=True),
    Field("pam", "string", length=3, default="", notnull=True),
    Field("kolmas", "string", length=1, default="", notnull=True),
    Field("mas", "string", length=100, default="", notnull=True),
    Field("par", "string", length=150, default="", notnull=True),
    Field("k", "string", length=5, default="", notnull=True),
    Field("rs485", "string", length=5, default="", notnull=True),
    Field("ist", "string", length=10, default="", notnull=True),
    Field("can", "string", length=10, default="", notnull=True),
    Field("ident", "string", length=32, default="", notnull=True),
    Field("type_ks", "string", length=3, default="", notnull=True),
    Field("d_lv", "string", length=10, default="", notnull=True),
    Field("h_lv", "string", length=2, default="", notnull=True),
    Field("d_zv", "string", length=10, default="", notnull=True),
    Field("h_zv", "string", length=2, default="", notnull=True),
    Field("find_m", "string", length=2, default="", notnull=True),
    Field("find_p", "string", length=2, default="", notnull=True),
    Field("find_a", "string", length=5, default="", notnull=True),
    Field("bszp", "string", length=2, default="", notnull=True),
    Field("d_tt", "string", length=10, default="", notnull=True),
    Field("t_tt", "string", length=5, default="", notnull=True),
    Field("n_tt", "string", length=1, default="", notnull=True),
    Field("l_tt", "string", length=1, default="", notnull=True),
    Field("freq", "string", length=5, default="", notnull=True),
    Field("mnoj", "string", length=3, default="", notnull=True),
    Field("cur485", "string", length=1, default="", notnull=True),
    Field("baudrate", "string", length=6, default="", notnull=True),
    Field("parity", "string", length=1, default="", notnull=True),
    Field("npu", "string", length=2, default="", notnull=True),
    Field("mse", "string", length=19, default="", notnull=True),
    migrate='ocom.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.ocom.obj_id.label = "Объект"
db.ocom.bus_id.label = "Шина"
db.ocom.com_id.label = "АдресСч"
db.ocom.addr.label = "КорАдр"
db.ocom.l_addr.label = "РасшАдр"
db.ocom.ser_num.label = "СерНом"
db.ocom.tip.label = "ТипСЧ"
db.ocom.ver_po.label = "ВерсияПО"
db.ocom.point.label = "ТочУчёта"
db.ocom.date_izg.label = "ДатаИзг"
db.ocom.edi.label = "ЕдИзм"
db.ocom.tmpr.label = "Темпер"
db.ocom.k_tr_u.label = "КтрНапр"
db.ocom.k_tr_i.label = "КтрТок"
db.ocom.k_tr_cur.label = "КтрТек"
db.ocom.cla.label = "КлассАкт"
db.ocom.clr.label = "КлассРеак"
db.ocom.unom.label = "Uном"
db.ocom.inom.label = "Iном"
db.ocom.aconst.label = "Аконст"
db.ocom.tmp.label = "Тдиап"
db.ocom.faz.label = "Фаз"
db.ocom.t_fix.label = "МомФикс"
db.ocom.ti1.label = "ВрИнтег1"
db.ocom.date1.label = "ДтТекСр1"
db.ocom.time1.label = "ВрТекСр1"
db.ocom.pntr1.label = "АдТекСр1"
db.ocom.ti2.label = "ВрИнтег2"
db.ocom.date2.label = "ДтТекСр2"
db.ocom.time2.label = "ВрТекСр2"
db.ocom.pntr2.label = "АдТекСр2"
db.ocom.ti3.label = "ВрИнтег3"
db.ocom.date3.label = "ДтТекСр3"
db.ocom.time3.label = "ВрТекСр3"
db.ocom.pntr3.label = "АдТекСр3"
db.ocom.cnl.label = "ТипКанУч"
db.ocom.isp.label = "Исполн"
db.ocom.npr.label = "НапрУч"
db.ocom.pam.label = "РазмерПам"
db.ocom.kolmas.label = "КолМас"
db.ocom.mas.label = "ХарМас"
db.ocom.par.label = "ПарКомпл"
db.ocom.k.label = "Каналов"
db.ocom.rs485.label = "RS485"
db.ocom.ist.label = "РезИст"
db.ocom.can.label = "CAN"
db.ocom.ident.label = "Идентиф"
db.ocom.type_ks.label = "ТипКС"
db.ocom.d_lv.label = "ДатаЛВ"
db.ocom.h_lv.label = "ЧасЛВ"
db.ocom.d_zv.label = "ДатаЗВ"
db.ocom.h_zv.label = "ЧасЗВ"
db.ocom.find_m.label = "НайдМассив"
db.ocom.find_p.label = "НайдСтран"
db.ocom.find_a.label = "НайдАдрес"
db.ocom.bszp.label = "СостЗадачи"
db.ocom.d_tt.label = "ДатТТ"
db.ocom.t_tt.label = "ВрТТ"
db.ocom.n_tt.label = "НомТТ"
db.ocom.l_tt.label = "ЛимТТ"
db.ocom.freq.label = "Частота"
db.ocom.mnoj.label = "Множ485"
db.ocom.cur485.label = "НомТек485"
db.ocom.baudrate.label = "СкорШины"
db.ocom.parity.label = "Паритет"
db.ocom.npu.label = "ЧислоПерУср"
db.ocom.mse.label = "МомСбр"'''
#============================================================================
# Оперативная таблица - ПРОПУСК ЗАПРОСА
db.define_table( 
    "opas",
    Field("tip", "string", length=50, default="", notnull=True),
    Field("ver_po", "string", length=8, default="", notnull=True),
    Field("zap", "string", length=15, default="", notnull=True),
    migrate='opas.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.opas.tip.label = "ТипСч"
db.opas.ver_po.label = "ВерсияПО"
db.opas.zap.label = "Запрос"
#============================================================================
# Оперативная таблица - ПОКАЗАТЕЛИ КАЧЕСТВА ЭЛЕКТРИЧЕСТВА (ПКЭ)
db.define_table( 
    "opke",
    Field("obj_id", "reference obj"),
    Field("bus_id", "reference bus"),
    Field("sht_id", "reference sht"),
    Field("zap", "string", length=20, default="", notnull=True),
    Field("cdz_id", "reference cdz"),
    Field("cdp_id", "reference cdp"),
    Field("par_id", "reference par"),
    Field("t_int", "string", length=20, default="", notnull=True),
    Field("bot_lim", "string", length=20, default="", notnull=True),
    Field("top_lim", "string", length=20, default="", notnull=True),
    migrate='opke.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.opke.obj_id.label = "Объект"
db.opke.bus_id.label = "Шина"
db.opke.sht_id.label = "АдресСч"
db.opke.zap.label = "Запрос"
db.opke.cdz_id.label = "КодЗапр"
db.opke.cdp_id.label = "КодПарЗапр"
db.opke.par_id.label = "КодПар"
db.opke.cdp_id.label = "СокрНаимПок"
db.opke.t_int.label = "ВрИнтег"
db.opke.bot_lim.label = "НижнПредел"
db.opke.top_lim.label = "ВерхПредел"
#============================================================================
# Оперативная таблица - ЖУРНАЛЫ СОБЫТИЙ
db.define_table( 
    "ojrn",
    Field("obj_id", "reference obj"),
    Field("bus_id", "reference bus"),
    Field("sht_id", "reference sht"),
    Field("zap", "string", length=20, default="", notnull=True),
    Field("cdz_id", "reference cdz"),
    Field("cdp_id", "reference cdp"),
    Field("nz", "string", length=2, default=""),
    Field("dt1", "string", length=19, default="", notnull=True),
    Field("dt2", "string", length=19, default="", notnull=True),
    migrate='ojrn.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.ojrn.obj_id.label = "Объект"
db.ojrn.bus_id.label = "Шина"
db.ojrn.sht_id.label = "АдресСч"
db.ojrn.zap.label = "Запрос"
db.ojrn.cdz_id.label = "КодЗапр"
db.ojrn.cdp_id.label = "КодПарЗапр"
db.ojrn.nz.label = "НомЗаписи"
db.ojrn.dt1.label = "ДтВрВыклВых"
db.ojrn.dt2.label = "ДтВрВклВозвр"
#============================================================================
# Оперативная таблица - ПРОГРАММИРУЕМЫЕ ФЛАГИ
db.define_table( 
    "ompf",
    Field("obj_id", "reference obj"),
    Field("bus_id", "reference bus"),
    Field("sht_id", "reference sht"),
    Field("zap", "string", length=20, default="", notnull=True),
    Field("cdz_id", "reference cdz"),
    Field("cdp_id", "reference cdp"),
    Field("par_id", "reference par"),
    Field("b27", "string", length=1, default="", notnull=True), 
    Field("b26", "string", length=1, default="", notnull=True), 
    Field("b25", "string", length=1, default="", notnull=True), 
    Field("b24", "string", length=1, default="", notnull=True), 
    Field("b23", "string", length=1, default="", notnull=True),  
    Field("b22", "string", length=1, default="", notnull=True),  
    Field("b21", "string", length=1, default="", notnull=True),  
    Field("b20", "string", length=1, default="", notnull=True),  
    Field("b17", "string", length=1, default="", notnull=True),  
    Field("b16", "string", length=1, default="", notnull=True),  
    Field("b15", "string", length=1, default="", notnull=True),  
    Field("b14", "string", length=1, default="", notnull=True),  
    Field("b13", "string", length=1, default="", notnull=True),  
    Field("b12", "string", length=1, default="", notnull=True),  
    Field("b11", "string", length=1, default="", notnull=True),  
    Field("b10", "string", length=1, default="", notnull=True),
    migrate='ompf.table',
)    
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.ompf.obj_id.label = "Объект"
db.ompf.bus_id.label = "Шина"
db.ompf.sht_id.label = "АдресСч"
db.ompf.zap.label = "Запрос"
db.ompf.cdz_id.label = "КодЗапр"
db.ompf.cdp_id.label = "КодПарЗапр"
db.ompf.par_id.label = "КодПар"
db.ompf.b27.label = "Байт2Бит7"
db.ompf.b26.label = "Байт2Бит6"
db.ompf.b25.label = "Байт2Бит5"
db.ompf.b24.label = "Байт2Бит4"
db.ompf.b23.label = "Байт2Бит3"
db.ompf.b22.label = "Байт2Бит2"
db.ompf.b21.label = "Байт2Бит1"
db.ompf.b20.label = "Байт2Бит0"
db.ompf.b17.label = "Байт1Бит7"
db.ompf.b16.label = "Байт1Бит6"
db.ompf.b15.label = "Байт1Бит5"
db.ompf.b14.label = "Байт1Бит4"
db.ompf.b13.label = "Байт1Бит3"
db.ompf.b12.label = "Байт1Бит2"
db.ompf.b11.label = "Байт1Бит1"
db.ompf.b10.label = "Байт1Бит0"
#============================================================================
# Оперативная таблица - ПАРАМЕТРЫ ЭНЕРГИИ НАГРУЗКИ:
db.define_table( 
    "oenr",
    Field("obj_id", "reference obj"),
    Field("bus_id", "reference bus"),
    Field("sht_id", "reference sht"),
    Field("zap", "string", length=20, default="", notnull=True),
    Field("cdz_id", "reference cdz"),
    Field("cdp_id", "reference cdp"),
    Field("dt_beg", "string", length=19, default="", notnull=True),
    Field("dt_end", "string", length=19, default="", notnull=True),
    Field("tarif", "string", length=4, default="", notnull=True),
    Field("edi", "string", length=5, default="", notnull=True),
    Field("a_plus", "double", default=0, notnull=True),
    Field("a_minus", "double", default=0, notnull=True),
    Field("r_plus", "double", default=0, notnull=True),
    Field("r_minus", "double", default=0, notnull=True),
    Field("r1", "double", default=0, notnull=True),
    Field("r2", "double", default=0, notnull=True),
    Field("r3", "double", default=0, notnull=True),
    Field("r4", "double", default=0, notnull=True),
    Field("msk", "string", length=2, default="", notnull=True),
    migrate='oenr.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.oenr.obj_id.label = "Объект"
db.oenr.bus_id.label = "Шина"
db.oenr.sht_id.label = "АдресСч"
db.oenr.zap.label = "Запрос"
db.oenr.cdz_id.label = "КодЗапр"
db.oenr.cdp_id.label = "КодПарЗапр"
db.oenr.cdp_id.label = "ПерНакЭн"
db.oenr.dt_beg.label = "НачПер"
db.oenr.dt_end.label = "ОкнПер"
db.oenr.tarif.label = "Тариф"
db.oenr.edi.label = "ЕдИзм"
db.oenr.a_plus.label = "АктЭнПрям"
db.oenr.a_minus.label = "АктЭнОбр"
db.oenr.r_plus.label = "РеакЭнПрям"
db.oenr.r_minus.label = "РеакЭнОбр"
db.oenr.r1.label = "РеакЭн1кв"
db.oenr.r2.label = "РеакЭн2кв"
db.oenr.r3.label = "РеакЭн3кв"
db.oenr.r4.label = "РеакЭн4кв"
db.oenr.msk.label = "Маска"
#============================================================================
# Оперативная таблица - ПАРАМЕТРЫ МОЩНОСТИ НАГРУЗКИ
db.define_table( 
    "opwr",
    Field("obj_id", "reference obj"),
    Field("bus_id", "reference bus"),
    Field("sht_id", "reference sht"),
    Field("zap", "string", length=20, default="", notnull=True),
    Field("cdz_id", "reference cdz"),
    Field("cdp_id", "reference cdp"),
    Field("date", "string", length=10, default="", notnull=True),
    Field("time_beg", "string", length=8, default="", notnull=True),
    Field("time_end", "string", length=8, default="", notnull=True),
    Field("edi", "string", length=5, default="", notnull=True),
    Field("p_plus", "double", default=0, notnull=True),
    Field("p_minus", "double", default=0, notnull=True),
    Field("q_plus", "double", default=0, notnull=True),
    Field("q_minus", "double", default=0, notnull=True),
    Field("u_sred", "double", default=0, notnull=True),
    Field("i_sred", "double", default=0, notnull=True),
    Field("t_korp", "double", default=0, notnull=True),
    Field("attr", "string", length=150, default="", notnull=True),
    migrate='opwr.table',
)  
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.opwr.obj_id.label = "Объект"
db.opwr.bus_id.label = "Шина"
db.opwr.sht_id.label = "АдрСч"
db.opwr.zap.label = "Запрос"
db.opwr.cdz_id.label = "КодЗапр"
db.opwr.cdp_id.label = "КодПарЗапр"
db.opwr.date.label = "Дата"
db.opwr.time_beg.label = "ВрНач"
db.opwr.time_end.label = "ВрОкн"
db.opwr.edi.label = "ЕдИзм"
db.opwr.p_plus.label = "P+"
db.opwr.p_minus.label = "P-"
db.opwr.q_plus.label = "Q+"
db.opwr.q_minus.label = "Q-"
db.opwr.u_sred.label = "Us"
db.opwr.i_sred.label = "Is"
db.opwr.t_korp.label = "Temp"
db.opwr.attr.label = "Атрибуты"
#============================================================================
# Оперативная таблица - ПАРАМЕТРЫ ВРИ НАГРУЗКИ
db.define_table( 
    "owri",  
    Field("obj_id", "reference obj"),
    Field("bus_id", "reference bus"),
    Field("sht_id", "reference sht"),
    Field("zap", "string", length=20, default="", notnull=True),
    Field("cdz_id", "reference cdz"),
    Field("cdp_id", "reference cdp"),
    Field("par_id", db.par),
    Field("dt", "string", length=19, default="", notnull=True),
    Field("kvdr", "string", length=1, default="", notnull=True),
    Field("val", "string", length=20, default="", notnull=True),
    migrate='owri.table',
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.owri.obj_id.label = "Объект"
db.owri.bus_id.label = "Шина"
db.owri.sht_id.label = "АдресСч"
db.owri.zap.label = "Запрос"
db.owri.cdz_id.label = "КодЗапр"
db.owri.cdp_id.label = "КодПарЗапр"
db.owri.par_id.label = "КодПар"
db.owri.dt.label = "ДатаВремя"
db.owri.kvdr.label = "Квадрант"
db.owri.val.label = "ЗначПарам"
#============================================================================
'''
# Оперативная таблица - СПИСКИ ЗАПРОСОВ
db.define_table(
    "orqs",
    Field("im", "string", length=50, default="", unique=True),
    Field("naim", "string", length=1024, default="", notnull=True),
    Field("lst", "text"),
    Field("isx", "text"),
    Field('lst_up', 'upload', uploadfield='isx', notnull=True),
    migrate='orqs.table',
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.orqs.im.label = "ИмяСписка"
db.orqs.naim.label = "ОписСписка"
db.orqs.lst.label = "ВыбранЗапросы"
db.orqs.isx.label = "СодЗагрФайла"
db.orqs.lst_up.label = "ИзФайла"
#============================================================================
# Оперативная таблица - ОТЧЁТЫ
db.define_table(
  'orpt',
  Field("obj_id", "reference obj"),
  Field("snaim", "string", length=30, default="", notnull=True),
  Field("naim", "string", length=256, default="", notnull=True),
  Field("dt_beg", "string", length=19, default="", notnull=True),
  Field("dt_end", "string", length=19, default="", notnull=True),
  Field('image', 'blob'),
  migrate='orpt.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.orpt.obj_id.label = "Объект"
db.orpt.snaim.label = "СокрНаим"
db.orpt.naim.label = "НаимОтчёта"
db.orpt.dt_beg.label = "НачОтчПер"
db.orpt.dt_end.label = "ОкнОтчПер"
db.orpt.image.label = "Отчёт"
#============================================================================
'''
# Оперативная таблица - ПАРАМЕТРЫ ЭНЕРГИИ НАГРУЗКИ ПРИ ПУСКЕ
db.define_table(
  "oini",
  # Копия оперативной таблицы oenr ПАРАМЕТРЫ ЭНЕРГИИ
  db.oenr,
  #--------------------------------------------------------------------------
  migrate='oini.table'
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.oini.obj_id.label = "Объект"
db.oini.bus_id.label = "Шина"
db.oini.sht_id.label = "АдресСч"
db.oini.cdz_id.label = "КодЗапр"
db.oini.cdp_id.label = "КодПарЗапр"
db.oini.cdp_id.label = "ПерНакЭн"
db.oini.dt_beg.label = "НачПер"
db.oini.dt_end.label = "ОкнПер"
db.oini.tarif.label = "НомТарифа"
db.oini.a_plus.label = "АктЭнПрямПСТ"
db.oini.a_minus.label = "АктЭнОбрПСТ"
db.oini.r_plus.label = "РеакЭнПрямПСТ"
db.oini.r_minus.label = "РеакЭнОбрПСТ"
db.oini.r1.label = "РеакЭн1квПСТ"
db.oini.r2.label = "РеакЭн2квПСТ"
db.oini.r3.label = "РеакЭн3квПСТ"
db.oini.r4.label = "РеакЭн4квПСТ"
db.oini.msk.label = "Маска"
#============================================================================

#############################################################################
'''