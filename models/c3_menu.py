#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 25.02.2010
# Purpose: Модель - Главное меню приложения uCommun
#############################################################################
#                uCommun - Управление коммуникаторами
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
# Имя приложения
app = rq.application
#----------------------------------------------------------------------------
# Имя контроллера
ctr = rq.controller
#----------------------------------------------------------------------------
# Имя шаблона
view = rs.view
#----------------------------------------------------------------------------
# Заголовок приложения для базового шаблона
rs.title = "uCommun"
#----------------------------------------------------------------------------
# Подзаголовок приложения 
rs.subtitle = 'Управление коммуникаторами'
#----------------------------------------------------------------------------
# Автор приложения 
rs.author = "Беляков Анатолий Алексеевич"
#----------------------------------------------------------------------------
# Узнай больше о мета-атрибутах на сайте:
# http://dev.w3.org/html5/markup/meta.name.html
rs.meta.author = 'А.А.Беляков <aabelyakov@mail.ru>'
rs.meta.description = 'a cool new app'
rs.meta.keywords = 'web2py, python, framework'
rs.meta.generator = 'Web2py Web Framework'
rs.meta.copyright = 'Авторское право (c) А.А.Беляков 2010'
#============================================================================
ADMIN_MENU = False
#----------------------------------------------------------------------------
# Проверим, что последний вошедший пользователь является членом группы
# group_id = ss.iAdmId
ADMIN_MENU = auth.has_membership(ss.iAdmId)
#----------------------------------------------------------------------------
# Главное меню приложения для пользователей из группы usr
rs.menu = [
    ['Начальная страница', False,  URL(
        app,
        "default",
        "index")],

    ['Задать отчётный период', False,  URL(
        app,
        "reports",
        "rpt_dat")],
   
    ['Выгрузить отчёт', False, "", [
        ['Одиночные параметры', False, URL(
            app,
            'reports',
            'rpt_dwn_bas')],
        
        ['ПЧ энергия по объекту', False, URL(
            app,
            'reports',
            'rpt_dwn_obj')],
        
        ['ПЧ энергия по корпусу', False, URL(
            app,
            'reports',
            'rpt_dwn_krp')],
        
        ['ПЧ энергия по подразделению', False, URL(
            app,
            'reports',
            'rpt_dwn_pdr')],
        
        ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
            app,
            'reports',
            'rpt_dwn_lgh')],
        ]],
        
    ['Смотреть диаграмму', False, "", [
        ['ПЧ энергия по объекту', False, URL(
            app,
            'charts',
            'chr_vie_obj')],
        
        ['ПЧ энергия по корпусу', False, URL(
            app,
            'charts',
            'chr_vie_krp')],
        
        ['ПЧ энергия по подразделению', False, URL(
            app,
            'charts',
            'chr_vie_pdr')],
        
        ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
            app,
            'charts',
            'chr_vie_lgh')],
        ]],
    
    ['Выгрузить диаграмму', False, "", [
        ['ПЧ энергия по объекту', False, URL(
            app,
            'charts',
            'chr_dwn_obj')],
            
        ['ПЧ энергия по корпусу', False, URL(
            app,
            'charts',
            'chr_dwn_krp')],
            
        ['ПЧ энергия по подразделению', False, URL(
            app,
            'charts',
            'chr_dwn_pdr')],
        
        ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
            app,
            'charts',
            'chr_dwn_lgh')],
        ]],
]
#============================================================================
def adm_menu():
    # Главное меню приложения для пользователей из группы adm
    #------------------------------------------------------------------------
    rs.menu = [
        ['Начальная страница', False,  URL(
            app,
            "default",
            "index")],
    
        ['Список запросов', False, "", [
            ["Выбрать", False, URL(
                app,
                'requests',
                'rqs_sel')],
    
            ["Править", False, URL(
                app,
                'requests',
                'rqs_upd_atr')],
    
            ["Энергия", False, URL(
                app,
                "requests",
                "rqs_crt_enr")],
    
            ["Загрузить", False, URL(
                app,
                'requests',
                'rqs_upl')],
    
            ["Выгрузить", False, URL(
                app,
                "requests",
                "rqs_dwn")],
            ]],
    
        ['Исполнители', False, "", [
            ["Смотреть", False, URL(
                app,
                'workers',
                'wrk_view')],
            
            ["Запустить", False, URL(
                app,
                'workers',
                'wrk_start')],
    
            ["Удалить", False, URL(
                app,
                'workers',
                'wrk_del')],
            ]],
        
        ['Запустить опрос', False, URL(
            app,
            'opros',
            'opr_start')],
        
        ['Записи', False, URL(
            app,
            'records',
            'rec_opr')],
    
        ['Отчётный период', False,  URL(
            app,
            "reports",
            "rpt_dat")],
    
        ['Создать отчёт', False, "", [
            ['Базовые параметры', False, URL(
                app,
                'reports',
                'rpt_crt_bas')],
            
            ['ПЧ энергия по объекту', False, URL(
                app,
                'reports',
                'rpt_crt_obj')],
    
            ['ПЧ энергия по корпусу', False, URL(
                app,
                'reports',
                'rpt_crt_krp')],
    
            ['ПЧ энергия по подразделению', False, URL(
                app,
                'reports',
                'rpt_crt_pdr')],
            
            ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
                app,
                'reports',
                'rpt_crt_lgh')],
            ]],
        
        ['Выгрузить отчёт', False, "", [
            ['Базовые параметры', False, URL(
                app,
                'reports',
                'rpt_dwn_bas')],
            
            ['ПЧ энергия по объекту', False, URL(
                app,
                'reports',
                'rpt_dwn_obj')],
            
            ['ПЧ энергия по корпусу', False, URL(
                app,
                'reports',
                'rpt_dwn_krp')],
            
            ['ПЧ энергия по подразделению', False, URL(
                app,
                'reports',
                'rpt_dwn_pdr')],
            
            ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
                app,
                'reports',
                'rpt_dwn_lgh')],
            ]],
            
        ['Смотреть диаграмму', False, "", [
            ['ПЧ энергия по объекту', False, URL(
                app,
                'charts',
                'chr_vie_obj')],
            
            ['ПЧ энергия по корпусу', False, URL(
                app,
                'charts',
                '')],
            
            ['ПЧ энергия по подразделению', False, URL(
                app,
                'charts',
                'chr_vie_pdr')],
            
            ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
                app,
                'charts',
                'chr_vie_lgh')],
            ]],
        
        ['Выгрузить диаграмму', False, "", [
            ['ПЧ энергия по объекту', False, URL(
                app,
                'charts',
                'chr_dwn_obj')],
                
            ['ПЧ энергия по корпусу', False, URL(
                app,
                'charts',
                'chr_dwn_krp')],
            
            ['ПЧ энергия по подразделению', False, URL(
                app,
                'charts',
                'chr_dwn_pdr')],
            
            ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
                app,
                'charts',
                'chr_dwn_lgh')],
            ]],
        
        ['Экспортир. данные', False, "", [
            ["Из одной таблицы", False, URL(
                app,
                'csvfile',
                'csv_exp_one')],
    
            ['Изо всех справ. таблиц', False, URL(
                app,
                'csvfile',
                'csv_exp_all')],
            ]],
    
        ['Импортир. данные', False, "", [
            ['В одну таблицу', False, URL(
                app,
                'csvfile',
                'csv_imp_one')],
    
            ['Во все справ. таблицы', False, URL(
            app,
            'csvfile',
            'csv_imp_all')],
        ]],
        
        ['Удалить данные', False, "", [
            ["В одной таблице", False, URL(
                app,
                'edit',
                'edt_trn_one')],
    
            ['Во всех опер.таблицах', False, URL(
                app,
                'edit',
                'edt_trn_all')],
            ]],
   
        ['Править таблицу', False,  URL(
            app,
            "edit",
            "edt_sel_spr")],
    
        ['Смотреть таблицу', False,  URL(
            app,
            "edit",
            "edt_sel_tbl")],
        
        ['Отправить почту', False,  URL(
            app,
            "mail",
            "mai_pusk")],

        ['Править конфиг', False,  URL(
            app,
            "edit",
            "edt_cfg_par")],
        
        ['Чистить очередь', False,  URL(
            app,
            "opros",
            "opr_clr_que")],
                
        ['Повторитель', False, "", [
            ["Запустить", False, URL(
                app,
                'repeaters',
                'rep_start')],
            ]],
        
        ['Планировщик', False, "", [
            ["Смотреть", False, URL(
                app,
                'planners',
                'pln_view')],
            
            ["Запустить", False, URL(
                app,
                'planners',
                'pln_start')],
    
            ["Удалить", False, URL(
                app,
                'planners',
                'pln_del')],
            ]],
              
        ["Сайт", False, URL(
            "admin",
            "default",
            "site")],
            
        ['Тесты', False, "", [
            ['cdz-cdp', False, URL(
                app,
                'tests',
                'test1')],
                   
            ['cdz-cdp-par', False, URL(
                app,
                'tests',
                'test2')],
            
            ['Left', False, URL(
                app,
                'tests',
                'test3')],
            
            ['Xlsx', False, URL(
                app,
                'tests',
                'test24')],
            
            ['Множ.Выбор', False, URL(
                app,
                'tests',
                'test5')],

            ['Планировщик', False, URL(
                app,
                'tests',
                'test26')],
            
            ['HTML', False, URL(
                app,
                'tests',
                'test18')],
            
            ['CSV', False, URL(
                app,
                'tests',
                'test19')],
                        
            ['Sht', False, URL(
                app,
                'tests',
                'test28')],
            
            ['Log', False, URL(
                app,
                'tests',
                'test29')],
            
            ['XML', False, URL(
                app,
                'tests',
                'test30')],
            
            ['Geraldo', False, URL(
                app,
                'tests',
                'test4')],
            
            ['Комби', False, URL(
                app,
                'tests',
                'test6')],
            
            ['Почта', False, URL(
                app,
                'tests',
                'test7')],
            
            ['XML1', False, URL(
                app,
                'tests',
                'test8')],
            
            ['XML2', False, URL(
                app,
                'tests',
                'test9')],
        ]],
 
        ['Смотреть лог-файл', False,  URL(
            app,
            "edit",
            "edt_vie_log")],
    ]
#enddef    
#----------------------------------------------------------------------------
if ADMIN_MENU:
    # Установлен флаг - Показать меню администратора
    #------------------------------------------------------------------------
    # Вызов функции показа главного меню приложения для группы adm
    adm_menu()
#endif
#############################################################################

