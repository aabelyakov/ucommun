#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 23.12.2014
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                Авторское право (С) А.А.Беляков 2010
#############################################################################
import subprocess as sp
import os 
import signal
#############################################################################
@auth.requires_membership(role='admin')
def pln_start(): 
    """
    ИФК для запуска процесса-планировщика planner.py, который в заданное 
    время отправляет в очередь qplan на redis-сервере сообщения с данными
    сессии
    """
    #------------------------------------------------------------------------
    if ss.iPln:
        # Есть запущенные процессы планировщика
        #--------------------------------------------------------------------
        # Флеш-сообщение, которое будет выдано на странице назначения
        ss.flash = "Нельзя запустить более одного Планировщика"
        #--------------------------------------------------------------------
        # Перенаправление на страницу со списком PID исполнителей
        redirect(URL(r=rq, c="planners", f="pln_view"))
    #endif
    #------------------------------------------------------------------------
    # Создание полного пути к файлу планировщика planner.py
    path = os.path.join(os.getcwd(), 'planner.py') 
    #------------------------------------------------------------------------
    # Запускаемая команда
    command = "python %s" % path
    #------------------------------------------------------------------------
    # Запуск процесса shell и выполнение в нём команды command
    # close_fds - закрывает в порождённом процессе все файловые дескрипторы,
    # в том числе, и открытые сокеты, унаследованные от родительского 
    # процесса.
    # os.setpgrp - запускается после fork(), но перед exec(), то есть, перед
    # созданием процесса shell и делает этот процесс лидером в группе 
    # процессов, присваивая ему ID группы дочерних процессов
    proc = sp.Popen(
        command, 
        shell=True, 
        stdin=sp.PIPE, 
        stdout=sp.PIPE,
        stderr=sp.STDOUT, 
        close_fds=True,
        preexec_fn=os.setpgrp
    )
    #------------------------------------------------------------------------
    # Запоминание в сессии ID запущенного лидера группы дочерних процессов 
    ss.iPln1 = proc.pid
    #------------------------------------------------------------------------
    # Флеш-сообщение, которое будет выдано на странице назначения
    ss.flash = "Планировщик опроса запущен"
    #------------------------------------------------------------------------
    # Перенаправление на страницу со списком PID исполнителей
    redirect(URL(r=rq, c="planners", f="pln_view"))
#enddef
#============================================================================
@auth.requires_membership(role='admin')
def pln_del(): 
    """
    ИФК для удаления лидера c группой дочерних процессов по его ID
    """
    #------------------------------------------------------------------------
    if not ss.iPln:
        # Запущенных планировщиков нет
        #--------------------------------------------------------------------
        # Флеш-сообщение, которое будет выдано на странице назначения
        ss.flash = "Запущенные Планировщики отсутствуют"
        #--------------------------------------------------------------------
        # Перенаправление на страницу со списком PID исполнителей
        redirect(URL(r=rq, c="planners", f="pln_view"))
    #endif    
    #------------------------------------------------------------------------
    try:
        # Попытка выполнения выражения
        #--------------------------------------------------------------------
        # Удаление лидера с группой процессов по его ID
        os.killpg(ss.iPln, signal.SIGTERM)
        #--------------------------------------------------------------------
    except OSError, err:
        # Попытка выполнения выражения - неудачна
        #--------------------------------------------------------------------
        # Отправка в лог-файл info-сообщения
        Info(err)
        #--------------------------------------------------------------------
        # Удаление ID группы процессов из переменной ss.iPln
        ss.iPln = None
        #--------------------------------------------------------------------
    else:    
        # Попытка выполнения выражения - удачна
        #--------------------------------------------------------------------
        ss.flash = "Планировщик с PID=%s удалён" % ss.iPln
    #endtry    
    #------------------------------------------------------------------------
    # Перенаправление на страницу со списком PID исполнителей
    redirect(URL(r=rq, c="planners", f="pln_view"))
    #------------------------------------------------------------------------
    # Возврат словаря с данными для подстановки в шаблон страницы 
    return dict(
        zgl = "УДАЛЕНИЕ ПЛАНИРОВЩИКА",
        form1 = form
    )
#enddef
#============================================================================
def pln_view():
    """
    ИФК для просмотра ID Планировщика    
    """
    #------------------------------------------------------------------------
    # Запускаемая команда
    command = "ps -aef | grep '[p]lanner.py' | grep '/bin/sh'"  
    #------------------------------------------------------------------------
    # Запуск подпроцесса в фоновом режиме выполнение в нём команды command
    proc = sp.Popen(
        command, 
        shell=True, 
        stdin=sp.PIPE, 
        stdout=sp.PIPE,
        stderr=sp.STDOUT, 
        close_fds=True
    )
    #------------------------------------------------------------------------
    while True:
        # "Вечный" цикл
        #--------------------------------------------------------------------
        # Чтение одной строки из выходного потока подпроцесса
        out = proc.stdout.readline()
        #--------------------------------------------------------------------
        # Преобразование строки в список по разделителю "пустая строка".
        # Элементы списка, содержащие пустые строки, из списка удаляются
        lOut = out.split()
        #--------------------------------------------------------------------
        if out == '' and proc.poll() != None:
            # Есть признаки: "EOF - Конец файла" и "Процесс завершён"
            #----------------------------------------------------------------
            # Выход из цикла
            break
            #----------------------------------------------------------------
        else:
            # Нет признаков: "EOF - Конец файла" и "Процесс завершён"
            #----------------------------------------------------------------
            ss.iPln = int(lOut[1]) if len(lOut)>1 else None
        #endif
    #endwhile  
    #------------------------------------------------------------------------
    if ss.iPln and ss.iPln == ss.iPln1:
        # ID запущенного и найденного процессов совпадают, то есть 
        # Планировщик запущен
        #--------------------------------------------------------------------
        # Подстановка pid в строку
        sPln = "%s" % ss.iPln
        #--------------------------------------------------------------------
    else:
        # Планировщик не запущен
        #--------------------------------------------------------------------
        sPln = "Нет запущенных Планировщиков"
    #endif    
    #------------------------------------------------------------------------
    return dict(
        zgl = "ID ЗАПУЩЕННОГО ПЛАНИРОВЩИКА",
        sts1 = DIV(
            P(l.RqsIm(db, ss)),
            P(l.DatDiap(ss))
            ),
        form1 = sPln
    )
#enddef
#############################################################################
